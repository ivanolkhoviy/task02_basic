package com.ivanolkhoviy;

/**
 * There are all methods you need for working with interval.
 * Make Fibonacci sequence and another.
 *
 * @author Ivan Olkhoviy
 */

public class CheckNum {

    /**
     * <p>Method give you sum of even numbers </p>
     *
     * @param start is the begining of interval
     * @param end   is the ending of interval
     * @return sum of even numbers
     */
    static int getEvenSum(int start, int end) {
        int sum = 0;
        int[] arr = new int[end + 1];
        for (int i = start; i < arr.length; i++) {
            arr[i] = i;
            if (arr[i] % 2 == 0) {
                sum = sum + arr[i];
            }
        }
        return sum;
    }

    /**
     * <p>Method give you sum of odd numbers </p>
     *
     * @param start is the begining of interval
     * @param end   is the ending of interval
     * @return sum of odd numbers
     */
    static int getOddSum(int start, int end) {
        int sum = 0;
        int[] arr = new int[end + 1];
        for (int i = start; i < arr.length; i++) {
            arr[i] = i;
            if (arr[i] % 2 != 0) {
                sum = sum + arr[i];
            }
        }
        return sum;
    }

    /**
     * <p>Method give you list of odd numbers </p>
     *
     * @param start is the begining of interval
     * @param end   is the ending of interval
     */

    static void getOddList(int start, int end) {
        int[] arr = new int[end];
        for (int i = start; i < arr.length; i++) {
            arr[i] = i + 1;
            if (arr[i] % 2 != 0)
                System.out.print(arr[i] + "  ");
        }
        System.out.println();
    }

    /**
     * <p>Method give you list of even numbers </p>
     *
     * @param start is the begining of interval
     * @param end   is the ending of interval
     */
    static void getEvenList(int start, int end) {
        int[] arr = new int[end];
        for (int i = start; i < arr.length; i++) {
            arr[i] = i + 1;
            if (arr[i] % 2 == 0)
                System.out.print(arr[i] + "  ");
        }
        System.out.println();
    }

    /**
     * <p>Method calculate Fibonacci sequence
     * and return percent of odd and even
     * numbers in sequence </p>
     *
     * @param start is the first number of sequence
     * @param end   is the second number of sequence
     * @param size  is the size of sequence
     * @return array of sequence
     * Shows the percent of odd and even numbers
     */
    static int[] getFibonacciSequence(final int start, final int end, final int size) {
        int[] fibonacci = new int[size];
        int evenCount = 0;
        fibonacci[0] = start;
        fibonacci[1] = end;
        if (fibonacci[0] % 2 == 0) {
            evenCount++;
        }
        if (fibonacci[1] % 2 == 0) {
            evenCount++;
        }
        System.out.print(fibonacci[0] + " " + fibonacci[1] + " ");
        for (int i = 2; i < fibonacci.length; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            System.out.print(fibonacci[i] + " ");
            if (fibonacci[i] % 2 == 0) {
                evenCount++;
            }
        }
        int oddCount = size - evenCount;
        System.out.println(" ");
        System.out.print("Percent for even numbers =  ");
        System.out.print(String.format("%.2f", (double) evenCount / size));
        System.out.println(" ");
        System.out.print("Percent for odd numbers =  ");
        System.out.print(String.format("%.2f", (double) oddCount / size));
        return fibonacci;
    }


}
