package com.ivanolkhoviy;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * The application class.
 * <h1>Task02_Basic </h1>
 *
 * @author Ivan Olkhoviy
 * @version 1.0 08 Nov 2019
 */

public class Application {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws IOException if readline dont work
     */
    public static void main(String[] args) {


        System.out.println("Hello! Write start of  interval :");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int start = Integer.parseInt(reader.readLine());
            System.out.println("Write end of interval :");
            int end = Integer.parseInt(reader.readLine());
            /**
             * Checking numbers for condition of conformity
             */
            if (start > end) {
                System.out.println("You wrote incorrect number , start has to be less then end");
            } else {
                System.out.println("List of odd numbers :");
                /**
                 *Invoke methods for get lists of odd and even numbers
                 */
                CheckNum.getOddList(start, end);
                System.out.println("List of even numbers :");
                CheckNum.getEvenList(start, end);

                System.out.println("The sum of even numbers is " + CheckNum.getEvenSum(start, end));
                System.out.println();
                System.out.println("The sum of odd numbers is " + CheckNum.getOddSum(start, end));
                /**
                 * User have to write size of set for Fibonacci sequence
                 * And we check this number for condition of conformity
                 */
                System.out.println("Write size of set for  Fibonacci sequence :");
                int size = Integer.parseInt(reader.readLine());
                if (size <= 1) {
                    System.out.println("Incorrect number , please write number > 1");
                } else {
                    System.out.println("List of Fibonacci sequence :");
                    CheckNum.getFibonacciSequence(start, end, size);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
